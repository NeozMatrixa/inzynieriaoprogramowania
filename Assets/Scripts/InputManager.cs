﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public float throt;
    public float steer;
    public bool thl;
    public bool brake;

    // Update is called once per frame
    void Update()
    {
        brake = Input.GetKey(KeyCode.Space);
        thl = Input.GetKeyDown(KeyCode.L);
        throt = Input.GetAxis("Vertical");
        steer = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Exit"))
        {
            Application.Quit();
        }
    }
}