﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;

public class RearWheelDrive : MonoBehaviour
{
    private WheelCollider[] wheels;
    float _flSlipMem;
    float _frSlipMem;
    float _blSlipMem;
    float _brSlipMem;
    float _flStiffMem;
    float _frStiffMem;
    float _blStiffMem;
    float _brStiffMem;

    public float maxAngle = 20;
    public float maxTorque = 2000;
    public float breaksFront = 200;
    public float breaksRear = 3500;
    public float lowSpeedAngle = 10;
    public float highSpeedAngle = 1;

    public float speedfract;
    public float steerfract;

    public GameObject wheelLeft;
    public GameObject wheelRight;

    //public GameObject Camera;
    public GameObject fl;
    public GameObject fr;
    public GameObject bl;
    public GameObject br;
    public GameObject rearRightLight;
    public GameObject rearLeftLight;
    public GameObject frontRightLight;
    public GameObject frontLeftLight;

/*
    public void CameraFix(float angle, float steerSpeedFracture)
    {
        Camera.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView =
            Clamp(40 - (this.GetComponent<Rigidbody>().velocity.magnitude / 3), 50, 15);
        Camera.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineComposer>()
            .m_ScreenX = Mathf.Lerp(0.5f, (angle / -(1 + steerSpeedFracture)) + 0.5f, 0.02f);
    }
*/
    public void Start()
    {
        wheels = GetComponentsInChildren<WheelCollider>();

        for (int i = 0; i < wheels.Length / 2; ++i)
        {
            var wheel = wheels[i];

            // create wheel shapes only when needed
            if (wheelLeft != null)
            {
                Instantiate(wheelLeft, wheel.transform, true);
                // Destroy(wheelLeft);
            }
        }

        for (int i = 2; i < wheels.Length; ++i)
        {
            var wheel = wheels[i];

            // create wheel shapes only when needed
            if (wheelRight != null)
            {
                Instantiate(wheelRight, wheel.transform, true);
                // Destroy(wheelRight);
            }
        }
    }

    public void FixedUpdate()
    {
        speedfract = Input.GetAxis("Vertical");
        steerfract = Input.GetAxis("Horizontal");

        float torque = maxTorque * speedfract;
        float steerSpeedFracture = GetComponent<Rigidbody>().velocity.magnitude;

        foreach (WheelCollider wheel in wheels)
        {
            // --------------------------NAPED-----------------------------
            if (wheel.transform.localPosition.z > 0)
            {
                wheel.brakeTorque = 0;
                float currentSteerAngle = Mathf.Lerp(lowSpeedAngle, highSpeedAngle, steerSpeedFracture / 100);
                float angle = maxAngle * steerfract * currentSteerAngle / 10;
                wheel.steerAngle = angle;
                //CameraFix(angle, steerSpeedFracture);
            }

            if (GetComponent<InputManager>().brake)
            {
                SetSlip(1f, 1f, 2f, 2f, 1.5f, 1.5f, 1.5f, 1.5f);
                if (wheel.transform.localPosition.z > 0)
                {
                    wheel.brakeTorque = breaksFront;
                }

                if (wheel.transform.localPosition.z < 0)
                {
                    wheel.brakeTorque = breaksRear;
                }

                rearRightLight.GetComponent<Renderer>().material.SetVector("_EmissionColor", Color.red * 0.5f);
                rearLeftLight.GetComponent<Renderer>().material.SetVector("_EmissionColor", Color.red * 0.5f);
            }
            else
            {
                if (wheel.transform.localPosition.z < 0)
                {
                    wheel.brakeTorque = 0;
                    wheel.motorTorque = torque;
                    if (GetComponent<InputManager>().throt != 0)
                    {
                        SetSlip(
                            Mathf.Lerp(0.1f, this.GetComponent<Rigidbody>().velocity.magnitude, 0.01f),
                            Mathf.Lerp(0.1f, this.GetComponent<Rigidbody>().velocity.magnitude, 0.01f),
                            Mathf.Lerp(0.2f, this.GetComponent<Rigidbody>().velocity.magnitude * 1.2f, 0.01f),
                            Mathf.Lerp(0.2f, this.GetComponent<Rigidbody>().velocity.magnitude * 1.2f, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude
                                * torque / 2000, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude
                                * torque / 2000, 0.01f)
                        );
                    }
                    else
                    {
                        SetSlip(
                            Mathf.Lerp(0.1f, this.GetComponent<Rigidbody>().velocity.magnitude * 0.6f, 0.01f),
                            Mathf.Lerp(0.1f, this.GetComponent<Rigidbody>().velocity.magnitude * 0.6f, 0.01f),
                            Mathf.Lerp(0.2f, this.GetComponent<Rigidbody>().velocity.magnitude * 0.3f, 0.01f),
                            Mathf.Lerp(0.2f, this.GetComponent<Rigidbody>().velocity.magnitude * 0.3f, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude * 0.7f, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude * 0.7f, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude
                                * torque / 2000, 0.01f),
                            Mathf.Lerp(1.45f, -this.GetComponent<Rigidbody>().velocity.magnitude
                                * torque / 2000, 0.01f)
                        );
                    }

                    /*
                    if (this.GetComponent<Rigidbody>().velocity.magnitude < 20)
                    {
                        SetSlip(
                            0.1f, 0.1f, 0.2f, 0.2f,
                            1.25f, 1.25f, 1.25f, 1.25f);
                    }

                    if (this.GetComponent<Rigidbody>().velocity.magnitude > 20)
                    {
                        SetSlip(
                            0.5f, 0.5f, 0.8f, 0.8f,
                            1.0f, 1.0f, 1.0f, 1.0f);
                    }
                    */
                }

                rearRightLight.GetComponent<Renderer>().material.SetVector("_EmissionColor", Color.white * 0.5f);
                rearLeftLight.GetComponent<Renderer>().material.SetVector("_EmissionColor", Color.white * 0.5f);
            }

            if (wheelLeft)
            {
                Quaternion q;
                Vector3 p;
                wheel.GetWorldPose(out p, out q);

                Transform shapeTransform = wheel.transform.GetChild(0);
                shapeTransform.position = p;
                shapeTransform.rotation = q;
            }

            if (wheelRight)
            {
                Quaternion q;
                Vector3 p;
                wheel.GetWorldPose(out p, out q);

                Transform shapeTransform = wheel.transform.GetChild(0);
                shapeTransform.position = p;
                shapeTransform.rotation = q;
            }
        }
    }

    public void SetSlip(float flSlip, float frSlip, float blSlip, float brSlip, float flStiff, float frStiff,
        float blStiff, float brStiff)
    {
        WheelFrictionCurve FL1 = fl.GetComponent<WheelCollider>().sidewaysFriction;
        WheelFrictionCurve FR1 = fr.GetComponent<WheelCollider>().sidewaysFriction;
        WheelFrictionCurve BL1 = bl.GetComponent<WheelCollider>().sidewaysFriction;
        WheelFrictionCurve BR1 = br.GetComponent<WheelCollider>().sidewaysFriction;
        FL1.extremumSlip = Mathf.Lerp(_flSlipMem, flSlip, 0.001f);
        FR1.extremumSlip = Mathf.Lerp(_frSlipMem, frSlip, 0.001f);
        BL1.extremumSlip = Mathf.Lerp(_blSlipMem, blSlip, 0.001f);
        BR1.extremumSlip = Mathf.Lerp(_brSlipMem, brSlip, 0.001f);
        FL1.stiffness = Mathf.Lerp(_flStiffMem, flStiff, 0.001f);
        FR1.stiffness = Mathf.Lerp(_frStiffMem, frStiff, 0.001f);
        BL1.stiffness = Mathf.Lerp(_blStiffMem, blStiff, 0.001f);
        BR1.stiffness = Mathf.Lerp(_brStiffMem, brStiff, 0.001f);
        fl.GetComponent<WheelCollider>().sidewaysFriction = FL1;
        fr.GetComponent<WheelCollider>().sidewaysFriction = FR1;
        bl.GetComponent<WheelCollider>().sidewaysFriction = BL1;
        br.GetComponent<WheelCollider>().sidewaysFriction = BR1;
        _flSlipMem = flSlip;
        _frSlipMem = frSlip;
        _blSlipMem = blSlip;
        _brSlipMem = brSlip;
        _flStiffMem = flStiff;
        _frStiffMem = frStiff;
        _blStiffMem = blStiff;
        _brStiffMem = brStiff;
    }

    public static T Clamp<T>(T value, T max, T min)
        where T : IComparable<T>
    {
        T result = value;
        if (value.CompareTo(max) > 0)
            result = max;
        if (value.CompareTo(min) < 0)
            result = min;
        return result;
    }
}