﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusFinish : MonoBehaviour
{
    public bool goNext = false;

    // Update is called once per frame
    void Update()
    {
        if (goNext)
        {
            Time.timeScale = Mathf.Lerp(0.2f, 1f, 0.001f);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;

            GameObject.Find("TimerText").GetComponent<Text>().text = "";
            if (Input.GetKeyDown(KeyCode.Space) && goNext == true)
            {
                Time.timeScale = 1f;
                Time.fixedDeltaTime = Time.timeScale * 0.02f;

                Debug.Log("Ladowanie sceny");
                Loader.Load(Loader.Scene.MainMenu);
            }
        }
    }
}