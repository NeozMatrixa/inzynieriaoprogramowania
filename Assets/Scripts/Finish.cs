﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    private bool isAreaClean = true;
    private bool isBotinside = false;
    private bool isPlayerinside = false;
    private bool checkActivate = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isAreaClean = true;
            isPlayerinside = true;
            checkActivate = true;
            if (!isBotinside)
            {
                startStopCountdown();
                checkActivate = false;
            }
        }
        else
        {
            isAreaClean = false;
            isBotinside = true;
            startStopCountdown();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isPlayerinside = false;
            isAreaClean = false;
            startStopCountdown();
        }
        else
        {
            isPlayerinside = true;
            isBotinside = false;
            if (isPlayerinside)
            {
                isAreaClean = true;
                checkActivate = true;
            }
            else if (!isBotinside)
            {
                isAreaClean = false;
                startStopCountdown();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Crash"))
        {
            isBotinside = true;
            isAreaClean = false;
            startStopCountdown();
            //Debug.Log(isPlayerinside + " " + isBotinside);
        }

        if (other.gameObject.CompareTag("Player"))
        {
            isPlayerinside = true;
            if (isAreaClean && isPlayerinside && checkActivate)
            {
                startStopCountdown();
                checkActivate = false;
            }
        }
        else
        {
            isPlayerinside = false;
            isBotinside = true;
            if (isAreaClean)
            {
                startStopCountdown();
                isAreaClean = false;
            }
        }
    }

    void startStopCountdown()
    {
        if (isAreaClean)
        {
            GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.3f);
            GetComponent<CountDown>().countdownTime = 5;
            GetComponent<CountDown>().isPlayerWin = true;
            StartCoroutine(GetComponent<CountDown>().CountdownToStart());
        }
        else
        {
            GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.3f);
            GetComponent<CountDown>().countdownTime = 0;
            GetComponent<CountDown>().isPlayerWin = false;
        }
    }
}