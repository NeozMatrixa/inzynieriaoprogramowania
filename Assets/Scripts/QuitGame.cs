﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
 
public class QuitGame : MonoBehaviour
{
 
    public void QuitTheGame() {
        Debug.Log("EXIT GAME");

        EditorApplication.isPlaying = false;
        
        Application.Quit();
    }
 
}