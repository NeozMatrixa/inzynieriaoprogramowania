﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Loader
{
    public enum Scene
    {
        Steering,
        Loading,
        MainMenu,
    }

    private static Action onLoaderCallback;

    public static void Load(Scene scene)
    {
        Debug.Log("Ladowanie Loadingu...");
        SceneManager.LoadScene(Scene.Loading.ToString());
        
        onLoaderCallback = () =>
        {
            Debug.Log("Callback");
            SceneManager.LoadScene(scene.ToString());
        };
    }

    public static void LoaderCallback()
    {
        if (onLoaderCallback != null)
        {
            Debug.Log("Loader callback zmiana statusu");
            onLoaderCallback();
            onLoaderCallback = null;
        }
    }
}