﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Xsl;
using UnityEditor.Sprites;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    private void Start()
    {
        Dropdown dropdown = GetComponentInChildren<Dropdown>();
        Debug.Log("nazwa: " + dropdown.name);

        DropdownItemSelected(dropdown);

        dropdown.onValueChanged.AddListener(delegate { DropdownItemSelected(dropdown); });
    }


    void DropdownItemSelected(Dropdown dropdown)
    {
        int index = (dropdown.value - 5) * -1;

        QualitySettings.SetQualityLevel(index, true);
        Debug.Log(QualitySettings.currentLevel + " " + QualitySettings.GetQualityLevel());
    }
}