﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    public int countdownTime;
    public Text countdownDisplay;
    public bool isGameOn = false;
    public bool isPlayerWin = false;
    private GameObject finishStatus;
    private GameObject briefing;

    private void Start()
    {
        briefing = GameObject.Find("Briefing");
        finishStatus = GameObject.Find("StatusFinish");
        briefing.SetActive(true);
        finishStatus.SetActive(false);
    }

    public void StartTimer()
    {
        briefing.SetActive(false);
        countdownDisplay.text = "";
        StartCoroutine(CountdownToStart());
        isGameOn = false;
        isPlayerWin = false;
    }

    public IEnumerator CountdownToStart()
    {
        while (countdownTime > 0)
        {
            countdownDisplay.text = countdownTime.ToString();

            yield return new WaitForSeconds(1f);

            countdownTime--;
        }

        if (isPlayerWin && isGameOn)
        {
            finishStatus.SetActive(true);
            finishStatus.GetComponent<StatusFinish>().goNext = true;

            countdownDisplay.text = "WIN";
            isGameOn = false;
            foreach (GameObject item in GameObject.FindGameObjectsWithTag("Crash"))
            {
                item.transform.position = new Vector3(item.transform.position.x, item.transform.position.y - 0.5f,
                    item.transform.position.z);
            }
        }
        else if (!isPlayerWin && isGameOn)
        {
            countdownDisplay.text = "";
        }
        else
        {
            countdownDisplay.text = "Start";
            Physics.autoSimulation = true;
            Time.timeScale = 1f;
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            isGameOn = true;
            yield return new WaitForSeconds(1f);
            countdownDisplay.text = "";
        }
    }
}