﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Briefing : MonoBehaviour
{
    void Start()
    {
        Physics.autoSimulation = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            QualitySettings.vSyncCount = 0; //Przy włączeniu Vsync odswierzanie co 2 klatki
            Application.targetFrameRate = 60;
            GameObject.Find("TimerCD").GetComponent<CountDown>().StartTimer();
        }
    }
}